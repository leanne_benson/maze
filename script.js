// Set starting game message ;

  const map = [
      "WWWWWWWWWWWWWWWWWWWWW",
      "W   W     W     W W W",
      "W W W WWW WWWWW W W W",
      "W W W   W     W W   W",
      "W WWWWWWW W WWW W W W",
      "W         W     W W W",
      "W WWW WWWWW WWWWW W W",
      "W W   W   W W     W W",
      "W WWWWW W W W WWW W F",
      "S     W W W W W W WWW",
      "WWWWW W W W W W W W W",
      "W     W W W   W W W W",
      "W WWWWWWW WWWWW W W W",
      "W       W       W   W",
      "WWWWWWWWWWWWWWWWWWWWW"
    ];
    //////////
let playerMove = document.createElement("div");
playerMove.classList.add("playerMove");
//////// 
const displayBlock = function(blockChar, rowDiv, rowIndex, columnIndex){
    console.log(rowIndex)
    let blockDiv = document.createElement("div");
    blockDiv.classList.add("block")
    blockDiv.dataset.columnNumber = columnIndex
    blockDiv.dataset.rowNumber = rowIndex
    if (blockChar === "W") {
      blockDiv.classList.add("wall");
    } else if (blockChar === " ") {
      blockDiv.classList.add("floor");
    } 
    if (blockChar === "S") {
      blockDiv.classList.add("start");
      blockDiv.appendChild(playerMove);
    } else if (blockChar === "F") {
      blockDiv.classList.add("finish");
    }
    rowDiv.appendChild(blockDiv)
}
const displayRow = function(rowString, index) {
//create and display a row div
    const maze = document.querySelector("#maze")//could want to get this out of the function only if it becomes a problem
    let rowDiv = document.createElement('div')
    rowDiv.classList.add("row")
    maze.appendChild(rowDiv)
    
    for (let columnNumber = 0; columnNumber < rowString.length; columnNumber++) {
        console.log(columnNumber)
       displayBlock(rowString.charAt(columnNumber), rowDiv, index, columnNumber);
    }
}
map.forEach(displayRow)
///////
document.addEventListener("keydown", logKey);
function logKey(e) {
  var key = e.which;
  e.preventDefault();
  let playerPositionColumn = Number(playerMove.parentElement.dataset.columnNumber)
  let playerPositionRow = Number(playerMove.parentElement.dataset.rowNumber)
    let playerParent = playerMove.parentElement
  console.log(playerPositionColumn, playerPositionRow)
  /////////////////
  switch (key) {
    case 38: //arrow up
    let playerUp = document.querySelector(`[data-column-number='${playerPositionColumn}'][data-row-number='${playerPositionRow-1}']`)
      console.log(playerUp.className)
      if(playerUp.className === "block floor"){
        playerUp.appendChild(playerMove)  
        //console.log("test")
      }
      break;
    case 40: //arrow down
    let playerDown = document.querySelector(`[data-column-number='${playerPositionColumn}'][data-row-number='${playerPositionRow+1}']`)
      console.log(playerDown.className)
      if(playerDown.className === "block floor"){
        playerDown.appendChild(playerMove)  
        //console.log("test")
      }
      break;
    case 39: //arrow right
      let playerRight = document.querySelector(`[data-column-number='${playerPositionColumn+1}'][data-row-number='${playerPositionRow}']`)
      console.log(playerRight.className)
      if(playerRight.className === "block floor"){
        playerRight.appendChild(playerMove)  
        //console.log("test")
      } 
      if(playerRight.className === "block finish"){
          playerRight.appendChild(playerMove)
          alert("You've beaten the Maze!");
      }
      break;
    case 37: //arrow left
    let playerLeft = document.querySelector(`[data-column-number='${playerPositionColumn-1}'][data-row-number='${playerPositionRow}']`)
      console.log(playerLeft.className)
      if(playerLeft.className === "block floor"){
        playerLeft.appendChild(playerMove)  
        //console.log("test")
      }
      break;
  }
 }